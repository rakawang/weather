<?php
    header("Content-Type: application/json");
    header("Access-Control-Allow-Origin: *");

    // Select weather data for given parameters
    $sql = "SELECT *
    FROM weather
    WHERE city = '{$_GET['city']}'
    AND weather_when >= DATE_SUB(NOW(), INTERVAL 10 SECOND)
    ORDER BY weather_when DESC limit 1";

    $result = mysqli_query($conn, $sql);

    // echo "Searching in web ...";

    // If 0 record found
    if ($result->num_rows == 0) {
        $url = 'https://api.openweathermap.org/data/2.5/weather?q=' . $_GET['city'] . '&appid=d47ab11bbda6db0de7b1dbb0c6ab20ea&units=metric';
        

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  
        
        // Get data from openweathermap and store in JSON object
        $data = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $json = json_decode($data, true);

        // Fetch required fields
        $weather_description = $json['weather'][0]['description'];
        $weather_temperature = $json['main']['temp'];
        $weather_wind = $json['wind']['speed'];
        // $weather_when = date("Y-m-d H:i:s"); // now
        $humidity = $json['main']['humidity'];
        $city = $json['name'];

        // for uv index
        $min = 1;
        $max = 8;
        $uvIndex = rand($min,$max);

        // Build INSERT SQL statement
        $sql = "INSERT INTO `weather`(`weather_description`, `weather_temperature`, `weather_wind`, `weather_when`, `humidity`, `city`, `uvIndex`)
        VALUES ('{$weather_description}', {$weather_temperature}, {$weather_wind}, CURRENT_TIMESTAMP, {$humidity}, '{$city}', {$uvIndex})";

        // $insertData = "INSERT INTO `weather`(`weather_description`, `weather_temperature`, `weather_wind`, `weather_when`, `humidity`, `city`, `uvIndex`)
        // VALUES ('Rainy', 40, 30, CURRENT_TIMESTAMP, 50, 'Kathmandu', 2)";

        // Run SQL statement and report errors
        if (!$conn -> query($sql)) {
        echo("<h4>SQL error description: " . $conn -> error . "</h4>");
        }
    }  
?>