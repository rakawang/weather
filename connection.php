<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db2059755";
$table = "weather";


// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$db_selected = mysqli_select_db($conn, $dbname);

if(!$db_selected) {
    // Create database
  $sql = "CREATE DATABASE $dbname";
  if ($conn->query($sql) === TRUE) {
    echo "Database created successfully";
  } else {
    echo "Error creating database: " . $conn->error;
  }
}

// before 
// echo "Before <br>";

// display records of the table 
// $sql = "SELECT weather_description, weather_temperature, weather_wind, weather_when FROM $table";
// $selectResult = $conn->query($sql);

// if ($selectResult->num_rows > 0) {
//   // output data of each row
//   while($row = $selectResult->fetch_assoc()) {
//     // echo "id: " . $row["weather_description"]. " - Name: " . $row["weather_temperature"]. " " . $row["weather_wind"]." " . " - when: ".$row["weather_when"]. "<br>";
//   }
// } else {
//   echo "0 results";
// }

// drop table if exist
$query="DROP table if exists $table"; 
if ($conn->query($query)) { 
  // echo "Table $table deleted  ....". "<br>";
}else{
  echo $conn->error;
}

// check if table exists

// if($stmt = $conn->query("SHOW TABLES LIKE '$table' ")){
//   if($stmt->num_rows ==1 ){
//    echo "table is created ";
//    }else{
//    echo $conn->error;
//  }
//  }

// sql to create table
$createTable = "CREATE TABLE `db2059755`.`weather` ( `weather_description` VARCHAR(255) NOT NULL , `weather_temperature` FLOAT NOT NULL , `weather_wind` FLOAT NOT NULL , `weather_when` DATETIME NOT NULL, `humidity` FLOAT NOT NULL, `city` VARCHAR(100) NOT NULL, `uvIndex` INT NOT NULL  ) ENGINE = InnoDB";

if ($conn->query($createTable) === TRUE) {
  // echo "New table created successfully". "<br>";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}


// insert query
$insertData = "INSERT INTO `weather`(`weather_description`, `weather_temperature`, `weather_wind`, `weather_when`, `humidity`, `city`, `uvIndex`)
              VALUES ('Rainy', 40, 30, CURRENT_TIMESTAMP, 50, 'Kathmandu', 2), ('Sunny Cloudy', 13, 20, CURRENT_TIMESTAMP, 26.5, 'London', 8)";
if ($conn->query($insertData)) { 
  // echo "Data inserted in $table ....". "<br>";
}else{
  echo $conn->error;
}

// after
// echo "After <br>";

// display records of the table 
// $sql = "SELECT weather_description, weather_temperature, weather_wind, weather_when FROM $table";
// $selectResult = $conn->query($sql);

// if ($selectResult->num_rows > 0) {
//   // output data of each row
//   while($row = $selectResult->fetch_assoc()) {
//     // echo "id: " . $row["weather_description"]. " - Name: " . $row["weather_temperature"]. " " . $row["weather_wind"]." " . " - when: ".$row["weather_when"]. "<br>";
//   }
// } else {
//   echo "0 results";
// }

// First, check requested data is present and fresh
include('city-import.php');

// Execute SQL query
$mainSql = "SELECT *
            FROM weather
            WHERE city = '{$_GET['city']}'
            AND weather_when >= DATE_SUB(NOW(), INTERVAL 10 SECOND)
            ORDER BY weather_when DESC LIMIT 1";


$result = mysqli_query($conn, $mainSql);
$rows = array();
while($r = mysqli_fetch_assoc($result)) {
    $rows[] = $r;
}

print json_encode($rows);


$conn -> close();
?>