var button= document.querySelector(".Submit");
var ProvidedCity = document.querySelector(".City_name");
var main = document.querySelector("#name");
var temperature = document.querySelector(".temperature");
// var when = document.querySelector(".TimePeroid");
var Description = document.querySelector(".Description");
var WindSpeed = document.querySelector(".WindSpeed");
var Humidity = document.querySelector(".Humidity");
var searchForm = document.querySelector("#search-form");
var days = document.querySelector(".days")
var uvindex = document.querySelector(".uvindex")
var fresh = document.querySelector(".updated_time_peroid")
var weather_img = document.querySelector('#weather_img');

// for days of week and date 
var weekday=new Array(7);
weekday[0]="Sun";
weekday[1]="Mon";
weekday[2]="Tue";
weekday[3]="Wed";
weekday[4]="Thu";
weekday[5]="Fri";
weekday[6]="Sat";


  button.addEventListener('click', getWeather)

  document.addEventListener("DOMContentLoaded", function(event) { 
    getWeather();
  });

  // Get AM nd PM with time
  function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }
  
  function getWeather() {

    //  Check browser cache first, use if there and less than 10 seconds old
    var localTime = new Date(localStorage.time); 
    if(localStorage.time != null
      && localTime.getTime() + 10000 > Date.now()) {
        
        console.log("local");
        
        var time = localTime;
        var day = weekday[time.getDay()];
        var date = time.getDate();

        days.innerHTML = day + ", " + date;
        main.innerHTML = "City - "+ localStorage.city;
        Description.innerHTML = "Description - " + localStorage.description;
        uvindex.innerHTML = "UV Index - " + localStorage.uvindex;
        temperature.innerHTML =  localStorage.temperature + " &#176; C";
        WindSpeed.innerHTML = "Wind Speed - " + localStorage.WindSpeedAPI+ " km/h";
        Humidity.innerHTML = "Humidity - " + localStorage.HumidityAPI + " %";
        fresh.innerHTML = "Updated: " + formatAMPM(localTime);

    
    // No local cache, access network
    } else {
      // remove cjan from path
    fetch('http://localhost/cjan/connection.php?city=' + ProvidedCity.value)
    .then(response => response.json())
    .then(data => {

      console.log(data[0])

      var cityName = data[0].city;
      var temperatureValue = data[0].weather_temperature;
      var timePeroid = data[0].weather_when;
      var DescriptionValue = data[0].weather_description;
      var WindSpeedAPI = data[0].weather_wind;
      var HumidityAPI = data[0].humidity;
      var uv = data[0].uvIndex;

      // for uv index condition
      var index = new Array(4);
      index[0] = "Low";
      index[1] = "Moderate";
      index[2] = "High";
      index[3] = "Very High";
      index[4] = "Extreme";

      var index_condition = index[0];

      if (uv >=0 && uv <= 2) {
        index_condition = index[0];
      }
      else if (uv >=3 && uv <= 5) {
        index_condition = index[1];
      } 
      else if (uv >=6 && uv <= 7) {
        index_condition = index[2];
      } 
      else if (uv >=8 && uv <= 10) {
        index_condition = index[3];
      } 
      else if (uv >=11) {
        index_condition = index[4];
      }
       else {
        alert("Wrong uv index")
      }

      var time = new Date(timePeroid);
      var day = weekday[time.getDay()];
      var date = time.getDate();

      // for image
      if(DescriptionValue == "Sunny Cloudy") {
        weather_img.src = "../img/sunny.png";
      }
      else if (DescriptionValue == "Rainy") {
        weather_img.src = "../img/rainy.png";
      }
      
      days.innerHTML = day + ", " + date;
      main.innerHTML = "City - "+ cityName;
      Description.innerHTML = "Description - " + DescriptionValue;
      uvindex.innerHTML = "UV Index - " + index_condition;
      temperature.innerHTML =  temperatureValue + " &#176; C";
      WindSpeed.innerHTML = "Wind Speed - " + WindSpeedAPI+ " km/h";
      Humidity.innerHTML = "Humidity - " + HumidityAPI + " %";
      fresh.innerHTML = "Updated: " + formatAMPM(time);

      localStorage.city = cityName;
      localStorage.description = DescriptionValue;
      localStorage.uvindex = index_condition;
      localStorage.temperature = temperatureValue;
      localStorage.WindSpeedAPI = WindSpeedAPI;
      localStorage.HumidityAPI = HumidityAPI;
      localStorage.time = time; 
      
      console.log(time.getTime(), Date.now());
    })
    .catch(err => {
      alert("'" + ProvidedCity.value + "'" + " does not exist or spelled wrong. ");
      searchForm.reset();

    });

    }
  }